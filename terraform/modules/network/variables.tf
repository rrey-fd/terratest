variable "VPC_NAME" {
  default = "default-vpc-name"
}

variable "PUBLIC_SUBNET_NAME" {
  default = "default-public-subnet"
}

variable "PRIVATE_SUBNET_NAME" {
  default = "default-private-subnet"
}
