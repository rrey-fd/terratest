resource "google_compute_network" "vpc" {
  name                    = "${var.VPC_NAME}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet_public" {
  name          = "${var.PUBLIC_SUBNET_NAME}"
  ip_cidr_range = "10.0.1.0/24"
  network       = "${google_compute_network.vpc.name}"
}

resource "google_compute_subnetwork" "subnet_private" {
  name          = "${var.PRIVATE_SUBNET_NAME}"
  ip_cidr_range = "10.0.2.0/24"
  network       = "${google_compute_network.vpc.name}"
}
