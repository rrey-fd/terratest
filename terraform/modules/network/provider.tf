provider "google" {
  project = "fnacdarty-digital-factory"
  region  = "europe-west1"
  version = "2.3.0"
}
