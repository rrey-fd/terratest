terragrunt = {
  include {
    path = "${find_in_parent_folders()}"
  }
  terraform {
    source = "../../modules/network/"
  }
}

PUBLIC_SUBNET_NAME = "toto"
