terragrunt = {
  remote_state {
    backend = "gcs"
    config {
      bucket         = "fnacdarty-terraform-backend"
      prefix         = "rre/dev/${path_relative_to_include()}"
    }
  }
  terraform {
    extra_arguments "common_var" {
      commands = [
        "apply",
        "plan",
        "refresh",
        "destroy",
        "output"
      ]

      arguments = [
        "-var-file=${get_tfvars_dir()}/../../common.tfvars",
      ]
    }
  }
}
